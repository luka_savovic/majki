import React from 'react'

import AddSection from 'containers/Sidebar/Section/AddSection'
import TemplateSection from 'containers/Sidebar/Section/TemplateSection'
import { LayerSection } from 'containers/Sidebar/Section/LayerSection'
import DimensionSection from './Section/DimensionSection'

const Sidebar = () => {
  return (
    <>
      <div className='sidebar--container'>
        <DimensionSection />
        <TemplateSection />
        <AddSection />
        <LayerSection />
      </div>
    </>
  )
}

export default Sidebar
