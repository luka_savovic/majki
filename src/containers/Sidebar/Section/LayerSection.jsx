import React from 'react'

import Layers from 'containers/Sidebar/Layers'
import { Panel, PanelHeader } from 'containers/Sidebar/Layout/Panel'
import { Row } from 'containers/Sidebar/Layout/Row'

export const LayerSection = () => (
  <>
    <Panel>
      <Row>
        <PanelHeader>Layers</PanelHeader>
      </Row>
      <Layers />
    </Panel>
  </>
)
