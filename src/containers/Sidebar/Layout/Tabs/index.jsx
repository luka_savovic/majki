import React from 'react'
import s from './index.module.scss'

export const Tabs = ({ style, children }) => {
  return (
    <div className={s.tabs}>
      <ul className={s.tabsWrapper}>{children}</ul>
    </div>
  )
}

export const Tab = ({ onClick, activeTab, eventkey, style, children }) => {
  return (
    <li
      className={`${s.tab} ${activeTab === eventkey ? s.active : ''}`}
      eventkey={eventkey}
      onClick={onClick}
      style={{ ...style }}>
      {children}
    </li>
  )
}

// Tabs.propTypes = {
//   style: PropTypes.object,
//   children: PropTypes.node.isRequired
// }

// Tabs.propTypes = {
//   onClick: PropTypes.func.isRequired,
//   activeTab: PropTypes.string.isRequired,
//   eventkey: PropTypes.string.isRequired,
//   style: PropTypes.object,
//   children: PropTypes.string.isRequired
// }
