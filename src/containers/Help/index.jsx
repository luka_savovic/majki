import React, { useState, useEffect } from 'react'
import Markdown from 'markdown-to-jsx'
import s from './index.module.scss'
import { Add } from 'components/Icons'

const Help = () => {
  const [showModal, setShowModal] = useState(false)
  const [data, setData] = useState(null)

  const url = process.env.REACT_APP_OUTLINE_HOST + '/api/documents.info'
  const token = `?&token=${process.env.REACT_APP_OUTLINE_TOKEN}`
  const documentId = `&id=${process.env.REACT_APP_OUTLINE_DOC_ID}`

  useEffect(() => {
    fetch(url + token + documentId)
      .then(res => res.json())
      .then(res => setData(res.data))
  }, [])

  return (
    <>
      <div className={s.helpContainer}>
        <h1 onClick={() => setShowModal(true)}>Help</h1>
      </div>
      {showModal ? (
        <Modal {...{ setShowModal, data, url, documentId }} />
      ) : null}
    </>
  )
}

const Modal = ({ setShowModal, data, url, documentId }) => {
  return (
    <>
      <div className={s.modalContainer}>
        <div className={s.topBar}>
          <div className={s.left}>
            {/* <input type='text' placeholder='Search' /> */}
          </div>

          <div className={s.right}>
            <a
              href={`${url}/doc/${documentId}`}
              target='_blank'
              rel='noopener noreferrer'
              className={s.editButton}>
              Edit
            </a>
            <div className={s.closeButton} onClick={() => setShowModal(false)}>
              <Add />
            </div>
          </div>
        </div>
        <div className={s.innerModalContainer}>
          {data && (
            <Markdown className={s.markdownContainer}>{data.text}</Markdown>
          )}
        </div>
      </div>
      <div className={s.modalBgOverlay}></div>
    </>
  )
}

export default Help
