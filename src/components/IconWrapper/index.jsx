import React from 'react'
import s from './index.module.scss'

export const IconWrapper = ({
  children,
  className,
  showIcon = true,
  ...props
}) => (
  <div role='button' className={`${s.iconWrapper} ${className}`} {...props}>
    {showIcon && <>{children}</>}
  </div>
)
