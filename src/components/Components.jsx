import React from 'react'

export const ColorPicker = ({ onClick, colors, activeColor }) => (
  <div className='color_picker--container'>
    {colors.map(color => (
      <div
        key={color}
        className={`color ${color === activeColor ? 'active' : ''}`}
        style={{ background: color }}
        name={color}
        onClick={onClick}></div>
    ))}
  </div>
)

export const BackgroundColor = ({ color }) => (
  <div className='background-color' style={{ background: color }}></div>
)
