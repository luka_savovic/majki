import React, { useRef, useState, useEffect } from 'react'
import * as Icon from 'components/Icons'
import s from './index.module.scss'
import { Hamburger } from 'components/Icons'

export const Dropdown = ({ children, ...props }) => {
  const node = useRef(null)
  const [open, setOpen] = useState(false)

  const handleClick = e => {
    if (node.current && node.current.contains(e.target)) {
      return
    }
    setOpen(false)
  }

  useEffect(() => {
    document.addEventListener('mousedown', handleClick)
    return () => {
      document.removeEventListener('mousedown', handleClick)
    }
  }, [])

  return (
    <div ref={node} className={s.dropdownContainer} {...props}>
      {React.Children.map(children, child =>
        React.cloneElement(child, { open, setOpen })
      )}
    </div>
  )
}

export const DropdownIcon = ({ open, setOpen, children }) => (
  <button
    onClick={() => setOpen(!open)}
    className={`svg-icon--container
                ${s.dropdownIcon}
                ${open ? s.active : ''}
              `}>
    {children}
  </button>
)

export const DropdownList = ({ open, setOpen, children }) => (
  <>
    {open && (
      <ul
        className={`${s.dropdownList} ${s.positionRight}`}
        onClick={() => setOpen(false)}>
        {children}
      </ul>
    )}
  </>
)

export const DropdownListItem = ({ children, ...props }) => (
  <li className={s.dropdownListItem} {...props}>
    <a>{children}</a>
  </li>
)

export const DropdownMenu = ({ open, children }) => (
  <>
    {open && (
      <div className={`${s.dropdownMenu} ${s.positionRight}`}>{children}</div>
    )}
  </>
)
