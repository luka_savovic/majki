import reducer from '../reducers/tearoutReducer'
import * as types from '../actions/types'

describe('tearout reducers', () => {
  it('add tearout', () => {
    const initialState = { tearout: [] }
    const action = {
      type: types.TEAROUT__ADD
    }
    expect(reducer(initialState, action)).toMatchObject(/tearout/)
  })

  it('deletes tearout', () => {
    const payload = { id: '2', text: 'unique' }
    const initialState = { tearout: [payload] }
    const action = {
      type: types.TEAROUT__DELETE,
      payload
    }
    expect(reducer(initialState, action)).toEqual({ tearout: [] })
  })

  it('updates tearout scale', () => {
    const payload = '2'
    const action = {
      type: types.TEAROUT__UPDATE_SCALE,
      payload
    }
    expect(reducer({}, action)).toEqual({ scale: payload })
  })

  it('updates tearout text content', () => {
    const payload = { id: '2', text: 'unique' }
    const initialState = { tearout: [payload] }
    const action = {
      type: types.TEAROUT__UPDATE_CONTENT,
      payload
    }
    expect(reducer(initialState, action)).toEqual({ tearout: [payload] })
  })

  it('updates tearout horizontal align', () => {
    const payload = 'size'
    const action = {
      type: types.TEAROUT__UPDATE_H_ALIGN,
      payload
    }
    expect(reducer({}, action)).toEqual({ alignment: { horizontal: payload } })
  })

  it('updates tearout vertical align', () => {
    const payload = 'size'
    const action = {
      type: types.TEAROUT__UPDATE_V_ALIGN,
      payload
    }
    expect(reducer({}, action)).toEqual({ alignment: { vertical: payload } })
  })
})
