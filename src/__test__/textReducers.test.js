import reducer from '../reducers/textReducer'
import * as types from '../actions/types'

describe('text reducers', () => {
  it('updates text', () => {
    const payload = 'text'
    const action = {
      type: types.TEXT__UPDATE_TEXT,
      payload
    }
    expect(reducer({}, action)).toEqual({ text: payload })
  })

  it('updates text color', () => {
    const payload = 'color'
    const action = {
      type: types.TEXT__UPDATE_COLOR,
      payload
    }
    expect(reducer({}, action)).toEqual({ color: payload })
  })

  it('updates text font', () => {
    const payload = 'font'
    const action = {
      type: types.TEXT__UPDATE_FONT,
      payload
    }
    expect(reducer({}, action)).toEqual({ font: payload })
  })

  it('updates text lineheight', () => {
    const payload = 'lineheight'
    const action = {
      type: types.TEXT__UPDATE_LINEHEIGHT,
      payload
    }
    expect(reducer({}, action)).toEqual({ lineheight: payload })
  })

  it('updates text size', () => {
    const payload = 'size'
    const action = {
      type: types.TEXT__UPDATE_SIZE,
      payload
    }
    expect(reducer({}, action)).toEqual({ size: payload })
  })

  it('updates text horizontal align', () => {
    const payload = 'size'
    const action = {
      type: types.TEXT__UPDATE_H_ALIGN,
      payload
    }
    expect(reducer({}, action)).toEqual({ align: { horizontal: payload } })
  })

  it('updates text vertical align', () => {
    const payload = 'size'
    const action = {
      type: types.TEXT__UPDATE_V_ALIGN,
      payload
    }
    expect(reducer({}, action)).toEqual({ align: { vertical: payload } })
  })
})
