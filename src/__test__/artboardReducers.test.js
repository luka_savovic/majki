import reducer from '../reducers/artboardReducer'
import * as types from '../actions/types'

const initialState = {
  template: {
    name: 'facebook',
    height: 314,
    width: 600
  },
  backgroundColor: '#ff671f',
  hasLogo: false,
  logoAlignment: {
    horizontal: 'justify-end',
    vertical: 'items-end'
  }
}

describe('artboard reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(initialState, {})).toEqual({
      ...initialState
    })
  })

  it('should handle ARTBOARD__UPDATE_TEMPLATE', () => {
    const payload = {
      name: 'twitter',
      height: 400,
      width: 500
    }
    const action = {
      type: types.ARTBOARD__UPDATE_TEMPLATE,
      payload
    }
    const expectedState = {
      ...initialState,
      template: payload
    }

    expect(reducer({ ...initialState }, action)).toEqual(expectedState)
  })

  it('should handle ARTBOARD__UPDATE_BG_COLOR', () => {
    const payload = '#2098f5'
    const action = { type: types.ARTBOARD__UPDATE_BG_COLOR, payload }
    const expectedState = {
      ...initialState,
      backgroundColor: payload
    }

    expect(reducer({ ...initialState }, action)).toEqual(expectedState)
  })

  it('should handle ARTBOARD__TOGGLE_LOGO', () => {
    const action = { type: types.ARTBOARD__TOGGLE_LOGO }
    const expectedState = {
      ...initialState,
      hasLogo: true
    }

    expect(reducer({ ...initialState }, action)).toEqual(expectedState)
  })

  it('should handle ARTBOARD__UPDATE_H_ALIGN', () => {
    const payload = 'xyz'
    const action = { type: types.ARTBOARD__UPDATE_H_ALIGN, payload }

    const expectedState = {
      ...initialState,
      logoAlignment: {
        ...initialState.logoAlignment,
        horizontal: 'xyz'
      }
    }

    expect(reducer({ ...initialState }, action)).toEqual(expectedState)
  })

  it('should handle ARTBOARD__UPDATE_V_ALIGN', () => {
    const payload = 'xyz'
    const action = { type: types.ARTBOARD__UPDATE_V_ALIGN, payload }

    const expectedState = {
      ...initialState,
      logoAlignment: {
        ...initialState.logoAlignment,
        vertical: 'xyz'
      }
    }

    expect(reducer({ ...initialState }, action)).toEqual(expectedState)
  })
})
