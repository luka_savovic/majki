import reducer from '../reducers/layerReducer'
import * as types from '../actions/types'

describe('image reducer', () => {
  it('adds an image', () => {
    const payload = {
      id: '1',
      image: 'xyz',
      isVisible: true
    }
    const action = {
      type: types.LAYER__ADD_IMAGE,
      payload
    }
    expect(reducer([], action)).toHaveLength(1)
  })

  it('deletes an image', () => {
    const initialState = [{ id: '1' }, { id: '2' }]
    const payload = { id: '1' }
    const action = {
      type: types.LAYER__DELETE,
      payload
    }
    expect(reducer(initialState, action)).toHaveLength(1)
    expect(reducer(initialState, action)).toEqual([{ id: '2' }])
  })

  it('toggles image visibility', () => {
    const initialState = [{ id: '1', isVisible: true }]
    const payload = { id: '1', isVisible: true }
    const action = {
      type: types.LAYER__TOGGLE_VISIBILITY,
      payload
    }
    expect(reducer(initialState, action)).toEqual([
      { id: '1', isVisible: false }
    ])
  })
})
