import * as actions from '../actions/artboardActions'
import * as types from '../actions/types'

describe('actions', () => {
  it('should create an action to update the artboard', () => {
    const payload = {
      name: 'twitter',
      height: 314,
      width: 600
    }
    const expectedAction = {
      type: types.ARTBOARD__UPDATE_TEMPLATE,
      payload
    }
    expect(actions.updateArtboardTemplate(payload)).toEqual(expectedAction)
  })
})
