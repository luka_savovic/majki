import reducer from '../reducers/settingsReducer'
import * as types from '../actions/types'

describe('settings reducers', () => {
  it('toggles grid', () => {
    const initialState = { hasGrid: false }
    const action = {
      type: types.SETTINGS__TOGGLE_GRID
    }
    expect(reducer(initialState, action)).toMatchObject({ hasGrid: true })
  })
})
