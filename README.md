<h1>Michelangelo</h1>

<p align='center'>
  <img src="https://d68ej2dhhub09.cloudfront.net/image_20608_full.png" style="max-width: 70px">
</p>

<p align="center">
  | <b><a href="#introduction">Introduction</a></b>
  | <b><a href="#installation">Installation</a></b>
  | <b><a href="#deployment">Deployment</a></b>
  | <b><a href="#usage">Usage</a></b>
  | <b><a href="#tips">Tips</a></b>
  |
</p>

---

# Introduction
Michelangelo is a graphic tool that lets you create social sharing images without introducing the complexities of Photoshop. It's completely browser-based and has no backend.


**Live demo:** [michelangelo-public.netlify.app](https://michelangelo-public.netlify.app/)

## Features
- Templates with popular social sizes and graphics
- Photoshop-like layer system
- Restrict use to only your brand font and colours
- Adjust image brightness, contrast, saturation, blur and ominosity
- Adjust text size, font, leading and colours
- Add newspaper tearouts as a way to display news articles
- [Optional] Directly send to Slack to speed up review process with designers
- [Optional] Connect Outline (getoutline.com) to add helpful tips and tricks

<img src="https://d68ej2dhhub09.cloudfront.net/image_20613_full.png">

> Note: The app has been tested **only** in Chrome 78+. The behaviour may not be consistent in other browsers. If fonts do not work during export, try installing a local copy on your machine.

# Installation
```
git clone https://github.com/the-open/michelangelo.git
cd michelangelo
yarn install
cp .env.sample .env
yarn start
```
This will start a local server at `http://localhost:3000`

# Deployment

## CDN

Our current suggestion for deployment is to install the repo locally, and run `yarn build` after configuring it for your organisation. After that, you can copy the contents of the build folder to any static site host (for example, AWS S3). 

If you can figure out how to easily deploy to Netlify or Heroku, please submit a PR :)



## Usage

Add your credentials in `.env` file to use Send to Slack/Outline. If you're using this feature, please note that your secret will be public. We avoided this by IP-restricting access to the site. 
```
REACT_APP_SLACK_ACTIVATED='false'
REACT_APP_SLACK_CHANNEL_TEST=''
REACT_APP_SLACK_CHANNEL_PROD=''
REACT_APP_SLACK_CLIENT_ID=''
REACT_APP_SLACK_CLIENT_SECRET=''
REACT_APP_SLACK_REDIRECT_URI=''

REACT_APP_OUTLINE_ACTIVATED='false'
REACT_APP_OUTLINE_HOST=''
REACT_APP_OUTLINE_DOC_ID=''
REACT_APP_OUTLINE_TOKEN=''
```

Add your brand colours and fonts. Change the host to your S3 bucket or CDN.

```
REACT_APP_BRAND_LOGOS_HOST='https://cdn.getup.org.au/'
REACT_APP_BRAND_LOGOS='GetUp|getup-white.png,ColourCode|cc-white.png'
REACT_APP_BRAND_LOGO_DEFAULT='GetUp'

REACT_APP_COLORS='#ffffff,#333333,#000000,#ff671f,#2098f5,#41b9d8,#f466ba,#fff766'
REACT_APP_PRIMARY_BACKGROUND_COLOR='#ff671f'
REACT_APP_PRIMARY_TEXT_COLOR='#ffffff'

REACT_APP_FONT_OPTIONS='Open Sans,Oswald'
REACT_APP_FONT_DEFAULT='Open Sans'
```

Update these values to add logos for newspaper tearouts and default text inputs. Change the host to your S3 bucket or CDN.

```
REACT_APP_TEAROUT_HOST='https://cdn.getup.org.au/'
REACT_APP_TEAROUT_LOGOS='ABC|abc.png,The Guardian|theguardian.png,Seven News|7news.png'
REACT_APP_TEAROUT_LOGO_DEFAULT='abc'
REACT_APP_TEAROUT_TEXT_DEFAULT='Lorem ipsum'
REACT_APP_TEAROUT_DATE_DEFAULT='24 December 2018'

REACT_APP_TEXT_DEFAULT='Lorem ipsum'
REACT_APP_STRAPLINE_DEFAULT='LOREM IPSUM'
```

Enable to add one additional template size. Adding more than one will require changes in the code.

```
REACT_APP_CUSTOM_TEMPLATE='false'
REACT_APP_CUSTOM_TEMPLATE_NAME='Custom'
REACT_APP_CUSTOM_TEMPLATE_HEIGHT='400px'
REACT_APP_CUSTOM_TEMPLATE_WIDTH='600px'
```

If there's something you'd like to add in a hamburger menu like links to briefing docs and other helpful design docs. Keeping it blank `''` will remove the button.
```
REACT_APP_MENU_ITEMS='Title|link,Title|link'
```


# Tips

All the text boxes are markdown and HTML enabled.
- Wrap words in `**` to bold and `*` to italicize
- Add `<br>` for a line-break
- Wrap it in `<hl>` tags for highlights: `<hl>Highlight</hl>`. To change highlight colour:
  - `<hl color='orange'>Highlight</hl>`
  - `<hl color='#ff671f'>Highlight</hl>`
